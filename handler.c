#include <stdlib.h>
#include <fcntl.h>
#include <curl/curl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include "gfserver.h"
#include "content.h"

#include "gfserver-student.h"
extern void create_request(gfcontext_t *ctx, char *path);
extern pthread_mutex_t m;
extern pthread_cond_t c;


ssize_t handler_get(gfcontext_t *ctx, char *path, void* arg){
	pthread_mutex_lock(&m);
	printf("path = %s", path);
	create_request(ctx,path);
	pthread_cond_signal(&c);
	pthread_mutex_unlock(&m);
	return 0;
	
	
}

