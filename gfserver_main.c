#include <getopt.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/signal.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include "gfserver.h"
#include "content.h"
#include "steque.h"
#define BUFFER_SIZE 8803   
#define USAGE \
"usage:\n"                                                                    \
"  gfserver_main [options]\n"                                                 \
"options:\n"                                                                  \
"  -t [nthreads]       Number of threads (Default: 3)\n"                      \
"  -p [listen_port]    Listen port (Default: 8803)\n"                         \
"  -m [content_file]   Content file mapping keys to content files\n"          \
"  -h                  Show this help message.\n"                             \

/* OPTIONS DESCRIPTOR ====================================================== */
static struct option gLongOptions[] = {
  {"port",          required_argument,      NULL,           'p'},
  {"nthreads",      required_argument,      NULL,           't'},
  {"content",       required_argument,      NULL,           'm'},
  {"help",          no_argument,            NULL,           'h'},
  {NULL,            0,                      NULL,             0}
};


typedef struct request_t{
	gfcontext_t *ctx; 
	char *path;
} request_t;

static steque_t *queue; 

void create_request(gfcontext_t *ctx, char *path){
	printf("path = %s", path);
	request_t *request = malloc(sizeof(request_t));
	request->ctx = ctx;
	request->path = strdup(path);
	steque_enqueue(queue,request);
}

pthread_cond_t c = PTHREAD_COND_INITIALIZER;
extern ssize_t handler_get(gfcontext_t *ctx, char *path, void* arg);
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
static void _sig_handler(int signo){
  if (signo == SIGINT || signo == SIGTERM){
    exit(signo);
  }
}

void *new_handler(void *worker){
	int fildes;
	size_t file_len, bytes_transferred;
	ssize_t read_len, write_len;
	char buffer[BUFFER_SIZE];
	
	while(1){
		
		pthread_mutex_lock (&m);
		while (steque_isempty(queue)){
			pthread_cond_wait(&c,&m);
		}
		
		request_t *newrequest = steque_pop(queue);
			pthread_mutex_unlock(&m);
			

			// old handler function starts//

	if( 0 > (fildes = content_get(newrequest->path)))
		gfs_sendheader(newrequest->ctx, GF_FILE_NOT_FOUND, 0);

	/* Determine the file size */
	file_len = lseek(fildes, 0, SEEK_END);

	gfs_sendheader(newrequest->ctx, GF_OK, file_len);

	/* Send the file in chunks */
	bytes_transferred = 0;
	while(bytes_transferred < file_len){
		read_len = pread(fildes, buffer, BUFFER_SIZE, bytes_transferred);
		if (read_len <= 0){
			fprintf(stderr, "handle_with_file read error, %zd, %zu, %zu\n", read_len, bytes_transferred, file_len );
			gfs_abort(newrequest->ctx);
			break;
		}
		write_len = gfs_send(newrequest->ctx, buffer, read_len);
		if (write_len != read_len){
			fprintf(stderr, "handle_with_file write error, %zd != %zd\n", write_len, read_len);
			gfs_abort(newrequest->ctx);
			break;
		}
		bytes_transferred += write_len;
	}
		
	free(newrequest->path);
	free(newrequest);

	}




}

/* Main ========================================================= */
int main(int argc, char **argv) {
  int option_char = 0;
  unsigned short port = 8803;
  char *content_map = "content.txt";
  gfserver_t *gfs = NULL;
  int nthreads = 3;
  long i;
	pthread_attr_t attr;
  if (signal(SIGINT, _sig_handler) == SIG_ERR){
    fprintf(stderr,"Can't catch SIGINT...exiting.\n");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGTERM, _sig_handler) == SIG_ERR){
    fprintf(stderr,"Can't catch SIGTERM...exiting.\n");
    exit(EXIT_FAILURE);
  }

  // Parse and set command line arguments
  while ((option_char = getopt_long(argc, argv, "m:p:t:h", gLongOptions, NULL)) != -1) {
    switch (option_char) {
      default:
        fprintf(stderr, "%s", USAGE);
        exit(1);
      case 'p': // listen-port
        port = atoi(optarg);
        break;
      case 't': // nthreads
        nthreads = atoi(optarg);
        break;
      case 'h': // help
        fprintf(stdout, "%s", USAGE);
        exit(0);
        break;       
      case 'm': // file-path
        content_map = optarg;
        break;                                          
    }
  }

  /* not useful, but it ensures the initial code builds without warnings */
  if (nthreads < 1) {
    nthreads = 1;
  }
  pthread_t workerthread[nthreads];

         
   /* Create threads to perform the dotproduct  */
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	

  
  content_init(content_map);

  /*Initializing server*/
  gfs = gfserver_create();
  


  /*Setting options*/
  gfserver_set_port(gfs, port);
  gfserver_set_maxpending(gfs, 64);
  gfserver_set_handler(gfs, handler_get);
  gfserver_set_handlerarg(gfs, NULL);
  queue = malloc(sizeof(steque_t));
  steque_init(queue);  


  for (i=0; i<nthreads ; i++){
		if ((pthread_create(&workerthread[i], &attr, new_handler, NULL)) != 0 ) {
			fprintf(stderr, "Unable to create worker thread\n");
			exit(1);
		}
	}
  /*Loops forever*/
  gfserver_serve(gfs);
}
